using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePhysics : MonoBehaviour
{
    Rigidbody myRigidBody;
    public float force;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
        force = 500;
    }

    // Update is called once per frame
    void Update()
    {
        float axisRight = Input.GetAxis("Horizontal");   // 1: right, 0: no button, -1: left
        float axisForward = Input.GetAxis("Vertical");
        Vector3 direction = transform.right * axisRight + transform.forward * axisForward;

        myRigidBody.MovePosition(transform.position + direction.normalized * speed * Time.deltaTime);
        // myRigidBody.velocity = direction.normalized * speed;
        // myRigidBody.AddForce(direction.normalized * speed * 10);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            myRigidBody.AddForce(Vector3.up * force);
        }
    }
}
