using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed;
    public float up_down;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(1, 1, 1);

        speed = 2;
        up_down = 3;
    }

    // Update is called once per frame
    void Update()
    {
        float axisRight = Input.GetAxis("Horizontal");   // 1: right, 0: no button, -1: left
        float axisForward = Input.GetAxis("Vertical");
        Vector3 direction = transform.right * axisRight + transform.forward * axisForward;
        transform.position += direction.normalized * speed * Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.Q))
        {
            transform.position += new Vector3(0, up_down, 0);
        }
        //Debug.Log(Time.deltaTime);
    }
}
