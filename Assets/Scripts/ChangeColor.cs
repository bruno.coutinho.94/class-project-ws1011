using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    MeshRenderer MeshRenderer;

    void Start()
    {
        MeshRenderer = GetComponent<MeshRenderer>();
    }

    public void SetColor(Color color)
    {
        MeshRenderer.material.color = color;
    }
}

