using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataAccess : MonoBehaviour
{
    public DataStorage storage;

    // Start is called before the first frame update
    void Start()
    {
        if (storage == null)
            storage = GetComponent<DataStorage>();
    }

    // Update is called once per frame
    void Update()
    {
        if (storage != null)
        {
            storage.Increment();
            storage.value += Time.deltaTime;
            Debug.Log(storage.value);
        }
    }
}
