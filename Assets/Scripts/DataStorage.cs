using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataStorage : MonoBehaviour
{
    public float value;

    public void Increment() {
        value += 1;
    }
}
