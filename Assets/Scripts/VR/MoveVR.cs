using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public class MoveVR : MonoBehaviour
{
    public int value;
    public float speed = 1;
    public bool canMove = true;

    public ChangeColor changeColor;
    public Transform directionReference;
    public HandRole hand;
    public ControllerAxis axisHorizontal;
    public ControllerAxis axisVertical = ControllerAxis.PadY;
    public ControllerButton button = ControllerButton.Trigger;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if (directionReference == null)
            directionReference = transform;
    }
    // Update is called once per frame
    void Update()
    {
        value = value + 1; //value++
        // transform.position += new Vector3(1,0,0) * speed * Time.deltaTime;
        float axisRight = ViveInput.GetAxis(hand, axisHorizontal); // 1: right 0: no button -1 : left
        float axisForward = ViveInput.GetAxis(hand, axisVertical);
        Vector3 direction = directionReference.right * axisRight + directionReference.forward * axisForward; //Vector3.right
        direction.y = 0;

        if (canMove)
        {
            transform.position += direction.normalized * speed * Time.deltaTime;
        }

        if (ViveInput.GetPressDown(hand, button))
        {
            Debug.Log("Pressed button");
        }

        if (direction != Vector3.zero)
            transform.forward = direction.normalized;

        //transform.rotation = Quaternion.Euler(0,90,0);
        //transform.LookAt(directionReference.position);
        // Debug.Log(Time.deltaTime);
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        Debug.Log("Collided with " + other.gameObject.name);
        if (other.gameObject.name == "Wall")
        {
            if (changeColor != null)
                changeColor.SetColor(Color.red);
        }
    }

    // If two objects with rigidbody collide. In which object would the function OnCollissionEnter be called?

    /// <summary>
    /// OnCollisionExit is called when this collider/rigidbody has
    /// stopped touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.name == "Wall")
        {
            if (changeColor != null)
                changeColor.SetColor(Color.white);
        }
    }
}